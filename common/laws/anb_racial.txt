﻿law_same_race_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_authority_add = 200
	}
	
	# cultural_acceptance_rule = {
	# 	is_same_race_as_any_primary_culture = { # Always accept your own race
	# 		#COUNTRY = prev.owner #errors
	# 		#country = ROOT			#doesnt error, but not sure it works
	# 		COUNTRY = scope:country
	# 	}
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	possible_political_movements = {
		law_monstrous_only
		law_non_monstrous_only
		law_giantkin_group_only
		law_goblinoid_group_only
		law_ruinborn_group_only
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_giantkin_group_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/land_based_taxation.dds"
	
	can_enact = {
		any_primary_culture = {
			is_giantkin_group = yes
		}
	}
	
	is_visible = {
		any_primary_culture = {
			is_giantkin_group = yes
		}
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	# cultural_acceptance_rule = {
	# 	is_giantkin_group = yes
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	modifier = {
		country_authority_add = 125
	}
	
	possible_political_movements = {
		law_same_race_only
		law_monstrous_only
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = {
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					this.culture = {
						is_giantkin_group = yes
					}
				}
				add = 0.5		
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					this.culture = {
						is_giantkin_group = yes
					}
					standard_of_living <= 10 
				}
				add = 0.5				
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_ruinborn_group_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/land_based_taxation.dds"
	
	can_enact = {
		any_primary_culture = {
			is_ruinborn_group = yes
		}
	}
	
	is_visible = {
		any_primary_culture = {
			is_ruinborn_group = yes
		}
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	# cultural_acceptance_rule = {
	# 	is_ruinborn_group = yes
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	modifier = {
		country_authority_add = 125
	}
	
	possible_political_movements = {
		law_same_race_only
		law_non_monstrous_only
		law_monstrous_only # Some ruinborn are monstrous some aren't
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = {
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					this.culture = {
						is_ruinborn_group = yes
					}
				}
				add = 0.5		
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					this.culture = {
						is_ruinborn_group = yes
					}
					standard_of_living <= 10 
				}
				add = 0.5				
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_goblinoid_group_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/land_based_taxation.dds"
	
	can_enact = {
		any_primary_culture = {
			is_goblinoid_group = yes
		}
	}
	
	is_visible = {
		any_primary_culture = {
			is_goblinoid_group = yes
		}
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	# cultural_acceptance_rule = {
	# 	is_goblinoid_group = yes
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	modifier = {
		country_authority_add = 125
	}
	
	possible_political_movements = {
		law_same_race_only
		law_monstrous_only
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = {
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					this.culture = {
						is_goblinoid_group = yes
					}
				}
				add = 0.5		
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					this.culture = {
						is_goblinoid_group = yes
					}
					standard_of_living <= 10 
				}
				add = 0.5				
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_monstrous_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/land_based_taxation.dds"
	
	can_enact = {
		any_primary_culture = {
			is_monstrous_culture = yes
		}
	}
	
	is_visible = {
		any_primary_culture = {
			is_monstrous_culture = yes
		}
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	# cultural_acceptance_rule = {
	# 	is_monstrous_culture = yes
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	modifier = {
		country_authority_add = 100
	}
	
	possible_political_movements = {
		law_same_race_only
		law_all_races_allowed
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = {
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
				}
				add = 0.5				
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					standard_of_living <= 10 
				}
				add = 0.5				
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_non_monstrous_only = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/land_based_taxation.dds"
	
	can_enact = {
		any_primary_culture = {
			is_non_monstrous_culture = yes
		}
	}
	
	is_visible = {
		any_primary_culture = {
			is_non_monstrous_culture = yes
		}
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	# cultural_acceptance_rule = {
	# 	is_non_monstrous_culture = yes
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	modifier = {
		country_authority_add = 100
	}
	
	possible_political_movements = {
		law_same_race_only
		law_all_races_allowed
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
				}
				add = 0.5				
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { has_law = law_type:law_same_race_only }
					standard_of_living <= 10 
				}
				add = 0.5
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_all_races_allowed = {
	group = lawgroup_racial_tolerance
	
	icon = "gfx/interface/icons/law_icons/land_based_taxation.dds"
	
	unlocking_laws = {
		law_slavery_banned
		law_debt_slavery
	}
	
	# cultural_acceptance_rule = {
	# 	hidden_trigger = {
	# 		is_accepted_culture = {
	# 			COUNTRY = scope:country
	# 		}
	# 	}
	# }
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_authority_add = 50
	}
	
	possible_political_movements = {
		law_monstrous_only
		law_non_monstrous_only
		law_same_race_only
	}
	
	pop_support = {
		value = 0
		
		add = {
			desc = "POP_DISCRIMINATED"
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { 
						NOT = { has_law = law_type:law_all_races_allowed }
					}
				}
				add = 0.5				
			}
			if = {
				limit = { 
					NOT = {
						pop_race_accepted = {
							COUNTRY = owner
						}
					}
					owner = { NOT = { has_law = law_type:law_all_races_allowed } }
					standard_of_living <= 10 
				}
				add = 0.5				
			}			
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}