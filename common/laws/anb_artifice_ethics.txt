﻿law_amoral_artifice_banned = {
	group = lawgroup_artificer_ethics
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
		
	modifier = {
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_pragmatic_artifice
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_pragmatic_artifice = {
	group = lawgroup_artificer_ethics
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_traditional_magic_encouraged
		law_artifice_encouraged
		law_artifice_only
	}
	
	modifier = {
		country_influence_mult = -0.2
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_amoral_artifice_banned
		law_amoral_artifice_embraced
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}

law_amoral_artifice_embraced = {
	group = lawgroup_artificer_ethics
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_traditional_magic_encouraged
		law_artifice_encouraged
		law_artifice_only
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_influence_mult = -0.4
	}
	
	possible_political_movements = {
		law_pragmatic_artifice
		law_amoral_artifice_banned
	}

	ai_will_do = {
		exists = ruler
		ruler = {
			has_ideology = ideology_mad_scientist
		}
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { ai_has_enact_weight_modifier_journal_entries = yes }
			add = 750
		}
	}
}