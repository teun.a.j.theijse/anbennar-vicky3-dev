﻿CHARACTERS = {
	c:B41 = {
		create_character = {
			first_name = Awen
			last_name = Woods
			historical = yes
			ruler = yes
			age = 62
			interest_group = ig_intelligentsia
			ideology = ideology_traditionalist
			traits = {
				cautious reserved senile political_operator
			}
		}
		create_character = {
			first_name = Quenvan
			last_name = PLACEHOLDER
			historical = yes
			culture = cu:epednar
			age = 34
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_reformer
			traits = {
				political_operator pious innovative
			}
		}
		create_character = {
			first_name = Erela
			last_name = Aldham
			historical = yes
			female = yes
			age = 63
			interest_group = ig_rural_folk
			ig_leader = yes
			ideology = ideology_radical
			traits = {
				persistent charismatic experienced_orator
			}
		}
		create_character = {	#Doesn't appear in game???
			first_name = Lotsi
			last_name = PLACEHOLDER
			historical = yes
			culture = cu:epednar
			age = 38
			interest_group = ig_armed_forces
			is_general = yes
			hq = region_epednan_expanse
			ig_leader = yes
			ideology = ideology_jingoist
			traits = {
				ambitious brave expensive_tastes open_terrain_commander
			}
		}
		create_character = {	#Doesn't appear in game???
			first_name = Gelman
			last_name = Roarsfield
			historical = yes
			age = 34
			interest_group = ig_landowners
			is_general = yes
			ig_leader = yes
			ideology = ideology_pacifist
			traits = {
				reserved erudite supply_requisitions_expert
			}
		}
	}
}
