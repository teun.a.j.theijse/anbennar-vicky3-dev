﻿CHARACTERS = {
	c:R18 ?= {
		create_character = {
			first_name = "Iztue"
			last_name = "Wolfborn"
			historical = yes
			ruler = yes
			age = 34
			culture = cu:chimera_hobgoblin
			interest_group = ig_armed_forces
			ideology = ideology:ideology_jingoist_leader
			traits = {
				ambitious
				imposing
				basic_offensive_planner
			}
		}
	}
}
