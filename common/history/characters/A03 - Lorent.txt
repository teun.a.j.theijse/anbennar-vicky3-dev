﻿CHARACTERS = {
	c:A03 ?= {
		create_character = {
			first_name = "Castíne"
			last_name = sil_Rubenaire
			historical = yes
			ruler = yes
			female = yes
			age = 27
			#birth_date = 1763.9.15
			interest_group = ig_landowners
			ideology = ideology_royalist
			dna = dna_adeline_sil_rubenaire
			traits = {
				romantic ambitious
			}
		}
	}
}
