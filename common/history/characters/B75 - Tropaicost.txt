﻿CHARACTERS = {
	c:B75 ?= {
		create_character = {
			first_name = Arturo
			last_name = silna_Lioncost
			historical = yes
			ruler = yes
			noble = yes
			commander = yes
			age = 25
			interest_group = ig_armed_forces
			ideology = ideology_moderate
			dna = dna_arturo_silna_lioncost
			traits = {
				inspirational_orator popular_commander charismatic expensive_tastes
			}
			culture = cu:busilari
		}
	}
}
