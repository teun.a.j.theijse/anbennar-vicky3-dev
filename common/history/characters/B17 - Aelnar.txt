﻿CHARACTERS = {
	c:B17 ?= {
		create_character = {
			first_name = "Elissa"
			last_name = "Oathkeeper"
			historical = yes
			ruler = yes
			birth_date = 1374.4.23
			interest_group = ig_landowners
			#ig_leader = yes
			ideology = ideology_racial_purist
			traits = {
				ambitious psychological_affliction cruel
			}
			female = yes
		}
	}
}
