﻿BUILDINGS={
	s:STATE_SKURKHA_KYARD = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E02"
						levels=2
						region="STATE_SKURKHA_KYARD"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E02"
						levels=1
						region="STATE_SKURKHA_KYARD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:E02"
						levels=1
						region="STATE_SKURKHA_KYARD"
					}
					building={
						type="building_financial_district"
						country="c:E02"
						levels=3
						region="STATE_SKURKHA_KYARD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:E02"
						levels=1
						region="STATE_SKURKHA_KYARD"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

	s:STATE_OVTO_KIGVAL = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E02"
						levels=2
						region="STATE_OVTO_KIGVAL"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E02"
						levels=2
						region="STATE_OVTO_KIGVAL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:E02"
						levels=2
						region="STATE_OVTO_KIGVAL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
		}
	}

	s:STATE_KVAKEINOLBA = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E02"
						levels=2
						region="STATE_KVAKEINOLBA"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E02"
						levels=3
						region="STATE_KVAKEINOLBA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E02"
						levels=1
						region="STATE_KVAKEINOLBA"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:E02"
						levels=2
						region="STATE_KVAKEINOLBA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E02"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}

	s:STATE_DZIMOKLI = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:E02"
						levels=3
						region="STATE_DZIMOKLI"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:E02"
						levels=2
						region="STATE_DZIMOKLI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_coal_mine"
				add_ownership={
					building={
						type="building_coal_mine"
						country="c:E02"
						levels=1
						region="STATE_DZIMOKLI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_food_industry"
				add_ownership={
					country={
						country="c:E02"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_disabled_canning" "pm_bakery" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E02"
						levels=1
						region="STATE_DZIMOKLI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E02"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:E02"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}

	s:STATE_FOIRAKHIAN = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E02"
						levels=1
						region="STATE_FOIRAKHIAN"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:E02"
						levels=1
						region="STATE_FOIRAKHIAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
		}
	}

	s:STATE_SHEVROMRZGH = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E02"
						levels=1
						region="STATE_SHEVROMRZGH"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E02"
						levels=2
						region="STATE_SHEVROMRZGH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E02"
						levels=1
						region="STATE_SHEVROMRZGH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" }
			}
		}
	}

	s:STATE_CAUBHEAMEAS = {
		region_state:E02 = {
		}
		region_state:E06 = {
		}
	}

	s:STATE_SERPENT_GIFT = {
		region_state:E05 = {
			create_building={ #only state in FP with wheat
				building="building_wheat_farm" 
				add_ownership={
					building={
						type="building_wheat_farm"
						country="c:E05"
						levels=1
						region="STATE_SERPENT_GIFT"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_sugar_plantation"
				add_ownership={
					building={
						type="building_sugar_plantation"
						country="c:E05"
						levels=1
						region="STATE_SERPENT_GIFT"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_opium_plantation"
				add_ownership={
					building={
						type="building_opium_plantation"
						country="c:E05"
						levels=1
						region="STATE_SERPENT_GIFT"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_tobacco_plantation"
				add_ownership={
					building={
						type="building_tobacco_plantation"
						country="c:E05"
						levels=1
						region="STATE_SERPENT_GIFT"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_SARLUN_GOILUST = {
		region_state:E05 = {
		}
		region_state:E03 = {
		}
	}

	s:STATE_UGHEABAR = {
		region_state:E08 = {
		}
	}

	s:STATE_NAGLAIBAR = {
		region_state:E08 = {
		}
		region_state:E03 = {
		}
	}

	s:STATE_IRDU_AGEENEAS = {
		region_state:E07 = {
			create_building={
				building="building_tea_plantation"
				add_ownership={
					building={
						type="building_tea_plantation"
						country="c:E07"
						levels=1
						region="STATE_IRDU_AGEENEAS"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_GHANEERSP = {
		region_state:E09 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E09"
						levels=1
						region="STATE_GHANEERSP"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_lead_mine"
				add_ownership={
					building={
						type="building_lead_mine"
						country="c:E09"
						levels=1
						region="STATE_GHANEERSP"
					}
				}
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_lead_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

	s:STATE_SOCHULEAG = {
		region_state:E03 = {
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E03"
						levels=1
						region="STATE_SOCHULEAG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_ADOI_FILEANAN = {
		region_state:E03 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E03"
						levels=1
						region="STATE_ADOI_FILEANAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
		}
	}

	s:STATE_ORCHEKH = {
		region_state:E04 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E04"
						levels=1
						region="STATE_ORCHEKH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
		}
	}

	s:STATE_APORLAEN = {
		region_state:E04 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E04"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_tobacco_plantation"
				add_ownership={
					building={
						type="building_tobacco_plantation"
						country="c:E04"
						levels=2
						region="STATE_APORLAEN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_ZARMIKLON = {
		region_state:E01 = {
		}
	}

	s:STATE_BULREKAYIG = {
		region_state:E01 = {
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E01"
						levels=1
						region="STATE_BULREKAYIG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
		}
	}

	s:STATE_AKANDHIL = {
		region_state:E01 = {
		}
	}

	s:STATE_TOZGONREK = {
		region_state:E01 = {
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E01"
						levels=1
						region="STATE_TOZGONREK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
		}
	}

	s:STATE_ZOITAL = {
		region_state:E01 = {
		}
	}

	s:STATE_ZUURZOI = {
		region_state:E01 = {
			create_building={
				building="building_whaling_station"
				add_ownership={
					building={
						type="building_whaling_station"
						country="c:E01"
						levels=1
						region="STATE_ZUURZOI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" }
			}
		}
	}

	s:STATE_PEENADHI = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=2
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_apple_orchards" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E01"
						levels=2
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E01"
						levels=2
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_vineyard_plantation"
						country="c:E01"
						levels=3
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:E01"
						levels=1
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_textile_mills"
						country="c:E01"
						levels=3
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" }
			}
			create_building={
				building="building_shipyards"
				add_ownership={
					building={
						type="building_shipyards"
						country="c:E01"
						levels=1
						region="STATE_PEENADHI"
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding"  }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_GADHLUMO = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=3
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "pm_apple_orchards" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E01"
						levels=3
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_vineyard_plantation"
						country="c:E01"
						levels=1
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_dye_plantation"
				add_ownership={
					building={
						type="building_dye_plantation"
						country="c:E01"
						levels=4
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E01"
						levels=3
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:E01"
						levels=2
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_tooling_workshops"
						country="c:E01"
						levels=1
						region="STATE_GADHLUMO"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=6
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_PRIKOYOL = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=3
						region="STATE_PRIKOYOL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_apple_orchards" }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_silk_plantation"
						country="c:E01"
						levels=1
						region="STATE_PRIKOYOL"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E01"
						levels=4
						region="STATE_PRIKOYOL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:E01"
						levels=3
						region="STATE_PRIKOYOL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			create_building={
				building="building_prikoyol_canal"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_canal" }
			}
		}
	}

	s:STATE_YUKARON = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=4
						region="STATE_YUKARON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_apple_orchards" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E01"
						levels=3
						region="STATE_YUKARON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_silk_plantation"
						country="c:E01"
						levels=5
						region="STATE_YUKARON"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_vineyard_plantation"
						country="c:E01"
						levels=2
						region="STATE_YUKARON"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:E01"
						levels=3
						region="STATE_YUKARON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E01"
						levels=3
						region="STATE_YUKARON"
					}
					building={
						type="building_textile_mills"
						country="c:E01"
						levels=3
						region="STATE_YUKARON"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" }
			}
			create_building={
				building="building_university"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=4
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
		}
	}

	s:STATE_YUKAROYOL = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=2
						region="STATE_YUKAROYOL"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:E01"
						levels=13
						region="STATE_YUKAROYOL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_food_industry"
				add_ownership={
					building={
						type="building_food_industry"
						country="c:E01"
						levels=2
						region="STATE_YUKAROYOL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_disabled_canning" "pm_sweeteners" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_university"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
		}
	}

	s:STATE_QUSHYIL = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=3
						region="STATE_QUSHYIL"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_silk_plantation"
						country="c:E01"
						levels=2
						region="STATE_QUSHYIL"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_vineyard_plantation"
						country="c:E01"
						levels=1
						region="STATE_QUSHYIL"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_shipyards"
				add_ownership={
					building={
						type="building_shipyards"
						country="c:E01"
						levels=1
						region="STATE_QUSHYIL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding"  }
			}
			create_building={
				building="building_military_shipyards"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_glassworks"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E01"
						levels=2
						region="STATE_QUSHYIL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_KESH_GOLKHIN = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=3
						region="STATE_KESH_GOLKHIN"
					}
				}
				reserves=1
				activate_production_methods = { "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:E01"
						levels=1
						region="STATE_KESH_GOLKHIN"
					}
				}
				reserves=1
				activate_production_methods = { "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={ #The City of Frosted Glass
				building="building_glassworks"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E01"
						levels=5
						region="STATE_KESH_GOLKHIN"
					}
					building={
						type="building_glassworks"
						country="c:E01"
						levels=4
						region="STATE_KESH_GOLKHIN"
					}
				} 
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E01"
						levels=2
						region="STATE_KESH_GOLKHIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E01"
						levels=1
						region="STATE_KESH_GOLKHIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:E01"
						levels=3
						region="STATE_KESH_GOLKHIN"
					}
					building={
						type="building_paper_mills"
						country="c:E01"
						levels=2
						region="STATE_KESH_GOLKHIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_shipyards"
				add_ownership={
					building={
						type="building_shipyards"
						country="c:E01"
						levels=1
						region="STATE_KESH_GOLKHIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding"  }
			}
			create_building={
				building="building_university"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=13
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:E01"
						levels=3
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}

	s:STATE_SARTZ_NEISAR = {
		region_state:E01 = {
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_furniture_manufacturies"
						country="c:E01"
						levels=3
						region="STATE_SHIK_DAZAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E01"
						levels=3
						region="STATE_SARTZ_NEISAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=1
						region="STATE_SARTZ_NEISAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" }
			}
			create_building={
				building="building_whaling_station"
				add_ownership={
					building={
						type="building_whaling_station"
						country="c:E01"
						levels=1
						region="STATE_SARTZ_NEISAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" }
			}
			create_building={
				building="building_university"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
		}
	}

	s:STATE_SHIKENKHIIN = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=3
						region="STATE_SHIKENKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" }
			}
			create_building={
				building="building_coal_mine"
				add_ownership={
					building={
						type="building_coal_mine"
						country="c:E01"
						levels=1
						region="STATE_SHIKENKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_gold_mine"
				add_ownership={
					building={
						type="building_gold_mine"
						country="c:E01"
						levels=4
						region="STATE_SHIKENKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_glassworks"
				add_ownership={
					building={
						type="building_glassworks"
						country="c:E01"
						levels=2
						region="STATE_SHIKENKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:E01"
						levels=4
						region="STATE_SHIKENKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=9
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
		}
	}

	s:STATE_ZOI_KHORKHIIN = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=1
						region="STATE_ZOI_KHORKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:E01"
						levels=4
						region="STATE_ZOI_KHORKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:E01"
						levels=11
						region="STATE_ZOI_KHORKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_whaling_station"
				add_ownership={
					building={
						type="building_whaling_station"
						country="c:E01"
						levels=3
						region="STATE_ZOI_KHORKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" }
			}
			create_building={
				building="building_food_industry"
				add_ownership={
					building={
						type="building_food_industry"
						country="c:E01"
						levels=2
						region="STATE_ZOI_KHORKHIIN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_disabled_canning" "pm_sweeteners" "pm_manual_dough_processing" "pm_pot_stills" }
			}
		}
	}

	s:STATE_SHIK_DAZAR = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				add_ownership={
					building={
						type="building_rye_farm"
						country="c:E01"
						levels=3
						region="STATE_SHIK_DAZAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_potatoes" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:E01"
						levels=3
						region="STATE_SHIK_DAZAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_furniture_manufacturies"
						country="c:E01"
						levels=3
						region="STATE_SHIK_DAZAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_textile_mills"
						country="c:E01"
						levels=2
						region="STATE_SHIK_DAZAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" }
			}
			create_building={
				building="building_shipyards"
				add_ownership={
					building={
						type="building_shipyards"
						country="c:E01"
						levels=1
						region="STATE_SHIK_DAZAR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding"  }
			}
			create_building={
				building="building_university"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:E01"
						levels=5
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:E01"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:E01"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}

}