﻿POPULATION = {
	c:B59 = { #Themaria
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}

	c:B60 = { #Nortiochand
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_high = yes
	}

	c:B61 = { #Noo Coddoran
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_high = yes
	}

	c:B61 = { #Noo Oddansbay
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
}