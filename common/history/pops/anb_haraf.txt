﻿POPS = {
	s:STATE_STEEL_BAY = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 365000
			}
			create_pop = {
				culture = ruin_orc
				size = 65600
				pop_type = slaves
			}
			create_pop = {
				culture = ruin_orc
				size = 16400
			}
			create_pop = {
				culture = tinker_gnome
				size = 47000
			}
			create_pop = {
				culture = soot_goblin
				size = 34000
			}
			create_pop = {
				culture = steelscale_kobold
				size = 63000
			}
			create_pop = {
				culture = gawedi
				size = 42000
			}
		}
	}
	s:STATE_GREENHILL = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 163000
			}
			create_pop = {
				culture = ruin_orc
				size = 110240
				pop_type = slaves
			}
			create_pop = {
				culture = ruin_orc
				size = 15600
			}
			create_pop = {
				culture = gawedi
				size = 10000
			}
		}
	}
	s:STATE_RYAIL = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 62000
			}
			create_pop = {
				culture = ruin_orc
				size = 27200
				pop_type = slaves
			}
			create_pop = {
				culture = ruin_orc
				size = 6800
			}
		}
	}
	s:STATE_SCATTERED_ISLANDS = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 41000
			}
		}
	}
	s:STATE_BRISTAILEAN = {
		region_state:B03 = {
			create_pop = {
				culture = arbarani
				size = 100
			}
			create_pop = {
				culture = vanburian #bigger island on the east was conquered from VG
				size = 100
			}
		}
		region_state:A09 = {
			create_pop = {			
				culture = crager
				size = 100
			}
		}
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 31000
			}
		}
	}
	s:STATE_GUARDIAN_ISLANDS = {
		region_state:B08 = {
			create_pop = {
				culture = crager
				size = 98000
			}
			create_pop = {
				culture = kooras
				size = 35000
				split_religion = {
					kooras = {
						dai_nadeilhil = 1 #placeholder religion
					}
				}
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 36000
			}
			create_pop = {
				culture = kooras
				size = 23000
				split_religion = {
					kooras = {
						dai_nadeilhil = 1 #placeholder religion
					}
				}
			}
		}
	}
	s:STATE_TAMETER = {
		region_state:B70 = {
			create_pop = {
				culture = mayte
				size = 37000
			}
			create_pop = {
				culture = cheshoshi
				size = 23000
			}
		}
		region_state:B27 = {
			create_pop = {
				culture = mayte
				size = 82000
				split_religion = {
					mayte = {
						death_masks = 0.8
						regent_court = 0.2
					}
				}
			}
			create_pop = {
				culture = cheshoshi
				size = 62000
			}
			create_pop = {
				culture = minarian
				size = 84000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
			create_pop = {
				culture = neratican
				size = 53000
			}
		}
		region_state:B11 = {
			create_pop = {
				culture = epednar
				size = 2000
			}
			create_pop = {
				culture = mayte
				size = 8000
			}
		}
	}
	s:STATE_MINAR_YOLLI = {
		region_state:B09 = {
			create_pop = {
				culture = minarian
				size = 412000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
			create_pop = {
				culture = mayte
				size = 230000
				split_religion = {
					mayte = {
						corinite = 0.6
						regent_court = 0.3
						ravelian = 0.1
					}
				}
			}
			create_pop = {
				culture = pearlsedger
				size = 124000 
				split_religion = {
					pearlsedger = {
						regent_court = 0.85
						ravelian = 0.15
					}
				}
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = minarian
				size = 14000 
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
		}
		region_state:B70 = {
			create_pop = {
				culture = mayte
				size = 140600
			}
			create_pop = {
				culture = cheshoshi
				size = 24000
			}
		}
	}
	s:STATE_OZTENCOST = {
		region_state:B03 = {
			create_pop = {
				culture = crownsman
				size = 187000
			}
			create_pop = {
				culture = pearlsedger
				size = 134000
				split_religion = {
					pearlsedger = {
						regent_court = 0.85
						ravelian = 0.15
					}
				}
			}
		}
		region_state:B04 = {
			create_pop = {
				culture = derannic
				size = 77000
			}
			create_pop = {
				culture = minarian
				size = 68000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
			create_pop = {
				culture = pearlsedger
				size = 51000
				split_religion = {
					pearlsedger = {
						regent_court = 0.85
						ravelian = 0.15
					}
				}
			}
		}
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 165000
			}
			create_pop = {
				culture = pearlsedger
				size = 114000
				split_religion = {
					pearlsedger = {
						regent_court = 0.85
						ravelian = 0.15
					}
				}
			}
		}
	}
	s:STATE_TLACHIBAR = {
		region_state:B70 = {
			create_pop = {
				culture = mayte
				size = 127000
			}
			create_pop = {
				culture = cheshoshi
				size = 10500
			}
		}
		region_state:B11 = {
			create_pop = {
				culture = epednar
				size = 61000
			}
			create_pop = {
				culture = mayte
				size = 33000
			}
		}
		region_state:B09 = {
			create_pop = {
				culture = mayte
				size = 99000
				split_religion = {
					mayte = {
						corinite = 0.45
						regent_court = 0.1
						ravelian = 0.05
						death_masks = 0.4
					}
				}
			}
			create_pop = {
				culture = minarian
				size = 49000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
		}
	}
	s:STATE_AUVUL_TADIH = {
		region_state:B11 = {
			create_pop = {
				culture = haraf_ne
				size = 82000 
			}
			create_pop = {
				culture = epednar
				size = 39000 
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = haraf_ne
				size = 84000
			}
		}
		region_state:B09 = {
			create_pop = {
				culture = mayte
				size = 43000
			}
			create_pop = {
				culture = minarian
				size = 23000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
		}
		region_state:B79 = {
			create_pop = {
				culture = haraf_ne
				size = 2000
			}
		}
        region_state:G18 = {
			create_pop = {
				culture = haraf_ne
				size = 2750
			}
		}
	}
	s:STATE_NANI_NOLIHE = {
		region_state:G18 = {
			create_pop = {
				culture = haraf_ne
				size = 3500
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = haraf_ne
				size = 436000
				split_religion = {
					haraf_ne = {
						the_thought = 0.4
						dai_nadeilhil = 0.6 
					}
				}
			}
			create_pop = {
				culture = soot_goblin
				size = 24000
			}
		}
	}
	s:STATE_MANGROVY_COAST = {
		region_state:B71 = {
			create_pop = {
				culture = pearlsedger
				size = 67000
				split_religion = {
					pearlsedger = {
						regent_court = 0.85
						ravelian = 0.15
					}
				}
			}
			create_pop = {
				culture = crager
				size = 43000
			}
			create_pop = {
				culture = minarian
				size = 33000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
		}
		region_state:B73 = {
			create_pop = {
				culture = mayte
				size = 64000
			}
			create_pop = {
				culture = haraf_ne
				size = 23000
				religion = death_masks
			}
			create_pop = {
				culture = kooras
				size = 12000
				religion = death_masks
			}
		}
		region_state:B09 = {
			create_pop = {
				culture = pearlsedger
				size = 43000
				split_religion = {
					pearlsedger = {
						regent_court = 0.85
						ravelian = 0.15
					}
				}
			}
			create_pop = {
				culture = crager
				size = 12000
			}
			create_pop = {
				culture = minarian
				size = 47000
				split_religion = {
					minarian = {
						corinite = 0.75
						regent_court = 0.2
						ravelian = 0.05
					}
				}
			}
			create_pop = {
				culture = haraf_ne
				size = 35000
			}
		}
	}
	s:STATE_NIDI_BIKEHA = {
		region_state:B10 = {
			create_pop = {
				culture = haraf_ne
				size = 47000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = haraf_ne
				size = 38000
				split_religion = {
					haraf_ne = {
						the_thought = 0.4
						dai_nadeilhil = 0.6 
					}
				}
			}
			create_pop = {
				culture = steelscale_kobold
				size = 11000
			}
			create_pop = {
				culture = soot_goblin
				size = 8000
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = haraf_ne
				size = 313000
				split_religion = {
					haraf_ne = {
						the_thought = 0.45
						dai_nadeilhil = 0.55 
					}
				}
			}
			create_pop = {
				culture = steelscale_kobold
				size = 56000
			}
			create_pop = {
				culture = soot_goblin
				size = 35000 
			}
		}
	}
	s:STATE_ZURZUMEXIA = {
		region_state:B07 = {
			create_pop = {
				culture = steelscale_kobold
				size = 866000
				split_religion = {
					steelscale_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = soot_goblin
				size = 209000
			}
			create_pop = {
				culture = kooras
				size = 248000
				split_religion = {
					kooras = {
						the_thought = 0.7
						dai_nadeilhil = 0.3 #placeholder until Ja'akaiin is implemented
					}
				}
			}
			create_pop = {
				culture = tinker_gnome
				size = 78000
			}
		}
	}
	s:STATE_MESTIKARDU = {
		region_state:B07 = {
			create_pop = {
				culture = soot_goblin
				size = 746000
				split_religion = {
					soot_goblin = {
						the_thought = 0.85
						ravelian = 0.15
					}
				}
			}
			create_pop = {
				culture = steelscale_kobold
				size = 201000
			}
			create_pop = {
				culture = tinker_gnome
				size = 117000
			}
			create_pop = {
				culture = kooras
				size = 46000
				split_religion = {
					kooras = {
						the_thought = 0.3
						dai_nadeilhil = 0.7 #placeholder until Ja'akaiin is implemented
					}
				}
			}
		}
	}
	s:STATE_E_E_KIDILE = {
		region_state:B74 = {
			create_pop = {
				culture = haraf_ne
				size = 197000
			}
		}
		region_state:G17 = {
			create_pop = {
				culture = haraf_ne
				size = 2700
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = haraf_ne
				size = 89000
				split_religion = {
					haraf_ne = {
						the_thought = 0.6
						dai_nadeilhil = 0.4 
					}
				}
			}
			create_pop = {
				culture = haiwai
				size = 45000
				split_religion = {
					haiwai = {
						righteous_path = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = tinker_gnome
				size = 74000
			}
		}
		region_state:B76 = {
			create_pop = {
				culture = haiwai
				size = 316000
				split_religion = {
					haiwai = {
						righteous_path = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = caamas
				size = 46000
			}
			create_pop = {
				culture = tinker_gnome
				size = 56000
			}
			create_pop = {
				culture = haraf_ne
				size = 189000
				split_religion = {
					haraf_ne = {
						the_thought = 0.6
						dai_nadeilhil = 0.4 
					}
				}
			}
		}
	}
	s:STATE_GOOVRAZ = {
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 201000
			}
			create_pop = {
				culture = soot_goblin
				size = 228000
			}
			create_pop = {
				culture = kooras
				size = 83000
				split_religion = {
					kooras = {
						the_thought = 0.2
						dai_nadeilhil = 0.8 #placeholder until Ja'akaiin is implemented
					}
				}
			}
			create_pop = {
				culture = steelscale_kobold
				size = 223000
			}
		}
	}
	s:STATE_GOMMIOCHAND = {
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 519000
				split_religion = {
					tinker_gnome = {
						the_thought = 0.84
						ravelian = 0.08
						regent_court = 0.08
					}
				}
			}
			create_pop = {
				culture = soot_goblin
				size = 183000
			}
			create_pop = {
				culture = steelscale_kobold
				size = 115000
			}
			create_pop = {
				culture = kooras
				size = 101000
				split_religion = {
					kooras = {
						the_thought = 0.8
						dai_nadeilhil = 0.2 #placeholder until Ja'akaiin is implemented
					}
				}
			}
			create_pop = {
				culture = haiwai
				size = 56000
				split_religion = {
					haiwai = {
						righteous_path = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 55000
				split_religion = {
					creek_gnome = {
						ravelian = 0.2
						regent_court = 0.8
					}
				}
			}
		}
		region_state:B76 = {
			create_pop = {
				culture = haiwai
				size = 92000
				split_religion = {
					haiwai = {
						righteous_path = 0.8
						high_philosophy = 0.2
					}
				}
			}
			create_pop = {
				culture = tinker_gnome
				size = 38000
			}
		}
	}
	s:STATE_WORMWAY = {
		region_state:B05 = {
			create_pop = {
				culture = haraf_ne
				size = 4000
			}
		}	
        region_state:B79 = {
			create_pop = {
				culture = haraf_ne
				size = 800
			}
		}	
        region_state:G18 = {
            create_pop = {
				culture = haraf_ne
				size = 1200
			}
        }
		region_state:G16 = {
			create_pop = {
				culture = haraf_ne
				size = 4000
			}
		}
        region_state:G17 = {
			create_pop = {
				culture = haraf_ne
				size = 3000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = haraf_ne
				size = 2000
			}
			create_pop = {
				culture = tinker_gnome
				size = 5000
			}
			create_pop = {
				culture = steelscale_kobold
				size = 8000
			}
			create_pop = {
				culture = soot_goblin
				size = 1000
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = haraf_ne
				size = 7000
			}
		}
	}
	s:STATE_KITSIL_KINN = {
		region_state:G18 = {
			create_pop = {
				culture = haraf_ne
				size = 11000
			}
		}
        region_state:B79 = {
            create_pop = {
				culture = haraf_ne
				size = 5000
			}
        }
		region_state:B10 = {
			create_pop = {
				culture = haraf_ne
				size = 5400
			}
		}
	}
}