﻿DIPLOMACY = {
	c:A01 = { #Anbennar
		create_diplomatic_pact = {
			country = c:B16 #New Erngrove
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B03	#Calasanni Trade Company
			type = dominion
		}

		#Imperial States
		create_diplomatic_pact = {
			country = c:A68 #Arbaran
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:A37 #Silverforge
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:A44 #Moonhaven
			type = puppet
		}
	}
	
	c:A02 = { #Vivin Empire
		create_diplomatic_pact = {
			country = c:A05 #Bisan
			type = puppet
		}
	}
	
	c:A03 = { #Lorent

		create_diplomatic_pact = {
			country = c:A65 #Redglades
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:A15	#Portnamm
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:A75 #Rubyhold
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:Y32 #Lorentish Haless Company
			type = dominion
		}

		#Aelantir
		create_diplomatic_pact = {
			country = c:B14 #Endralliande
			type = dominion
		}
		create_diplomatic_pact = {
			country = c:B27 #Neratica
			type = dominion
		}
		create_diplomatic_pact = {
			country = c:B26 #Nur Ionnidar
			type = puppet
		}

		#Aelantir - looser Anbennar colonies
		create_diplomatic_pact = {
			country = c:B13 #Varionail
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:B12 #Pirendral
			type = protectorate
		}

	}

	c:B14 = { #Endralliande
		create_diplomatic_pact = {
			country = c:B15 #Torrisheah
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B18 #New Redglades
			type = puppet
		}
	}

	c:B27 = { #Neratica
		create_diplomatic_pact = {
			country = c:B09 #Minaria
			type = puppet
		}
	}

	c:A09 = {	#Busilar-Eborthil-Deshak
		create_diplomatic_pact = {
			country = c:A08
			type = personal_union
		}
		create_diplomatic_pact = {
			country = c:F15	#Ovdal Tungr
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:B75 #Verten
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B08 #New Lorincrag
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:C09 #Turtleback Island
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:Y33 #Busilar Haless Company
			type = dominion
		}
		create_diplomatic_pact = {
			country = c:L68 #Tvarateram
			type = puppet
		}
	}

	c:A06 = {	#Gnomish Hierarchy
		create_diplomatic_pact = {
			country = c:A07	#Reveria
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:A17	#Brecc
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:B61	#Noo Oddansbay
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B62	#Noo Coddorran
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:G05	#Kobildzexbirrix
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:G08	#Ollorland
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:G06	#Derhildia
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:G07	#Nyr Revrhavn
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:G09	#Tiera
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B59	#Themaria
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:B80	#Eordand Administration
			type = dominion
		}
	}

	c:A04 = {	#Northern League
		create_diplomatic_pact = {
			country = c:A18	#Bayvek
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:A19	#Vertesk
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B04	#Deranne Trade Company
			type = puppet
		}
		# create_diplomatic_pact = {	#these guys dont exist anymore
		# 	country = c:B68	#Westmarches
		# 	type = puppet
		# }
	}

	c:A22 = {	#Ancardia
		create_diplomatic_pact = {
			country = c:A24	#Newshire
			type = puppet
		}
	}
	
	c:B29 = { #Sarda Empire
		create_diplomatic_pact = {
			country = c:B30 #Ynngard
			type = vassal
		}
		create_diplomatic_pact = {
			country = c:B31 #Veykoda
			type = personal_union
		}
		create_diplomatic_pact = {
			country = c:B32 #Vestipynn
			type = vassal
		}
		create_diplomatic_pact = {
			country = c:B33 #Corynnpoel
			type = vassal
		}
	}
	
	c:B47 = { #Arganjuzorn
		create_diplomatic_pact = {
			country = c:B91 #Ranger Republic
			type = tributary
		}
	}
	
	c:C01 = { #Rezankand
		create_diplomatic_pact = {
			country = c:C10 #Malatel
			type = puppet
		}
	}
	
	c:C02 = { #Amadia
		create_diplomatic_pact = {
			country = c:C11 #Amadian Territory
			type = puppet
		}
	}
	
	c:C03 = { #Ozgarom
		create_diplomatic_pact = {
			country = c:C04 #Jibirae'n
			type = puppet
		}
	}
	
	c:C05 = { #Nur Dhanaenn
		create_diplomatic_pact = {
			country = c:C06 #Swamp Territories
			type = puppet
		}
	}
	
	c:C39 = { #Ameion
		create_diplomatic_pact = {
			country = c:C60 #Neor Empkeios
			type = vassal
		}
	}
	
	c:C45 = { #Nanru Nakar
		create_diplomatic_pact = {
			country = c:C63 #Vettakaramai
			type = vassal
		}
	}
	
	c:C40 = { #Clemetar
		create_diplomatic_pact = {
			country = c:C41 #Scenkar
			type = tributary
		}
	}
	
	c:C50 = { #Degithion
		create_diplomatic_pact = {
			country = c:C61 #Akretírchel
			type = tributary
		}
	}
	
	c:E01 = { #Lake Fed
		create_diplomatic_pact = {
			country = c:E03 #Zabutodask
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:E04 #Shivusdoyen
			type = tributary
		}
	}
	
	c:F01 = { #Jaddanzar Empire
		create_diplomatic_pact = {
			country = c:R01 #Nahana Jadd
			type = personal_union
		}
		create_diplomatic_pact = {
			country = c:R02 #Ghankedhen
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:D25 #Verkal Gulan
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:R03 #Tudhina
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:R20 #Raghamidesh
			type = vassal
		}
	}

	c:Y32 = { #Lorentish Haless Company
		create_diplomatic_pact = {
			country = c:Y13
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:Y14
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:Y16
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:R05
			type = puppet
		}
	}

	c:Y33 = { #Busilari Haless Company
		create_diplomatic_pact = {
			country = c:Y23
			type = dominion
		}
		create_diplomatic_pact = {
			country = c:Y34
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:Y21
			type = puppet
		}
	}

	c:Y24 = { #Jaya Raya
		create_diplomatic_pact = {
			country = c:Y25
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:Y26
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:Y27
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:Y28
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:Y29
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:Y30
			type = tributary
		}
		create_diplomatic_pact = {
			country = c:Y31
			type = tributary
		}
	}

	c:Y19 = { #Baihon Xinh
		create_diplomatic_pact = {
			country = c:Y18
			type = tributary
		}
	}

	c:B98 = { #Trollsbay
		create_diplomatic_pact = {
			country = c:B89 #Daxwell
			type = puppet
		}
		create_diplomatic_pact = {
			country = c:B21 #Ynnsmouth
			type = protectorate
		}
		create_diplomatic_pact = {
			country = c:B06 #Saamirses
			type = protectorate
		}
	}

	c:D05 = { #Amldihr
		create_diplomatic_pact = {
			country = c:D03 #Dur-Vazhatun
			type = puppet
		}
	}

	c:D06 = { #Kuxheztë
		create_diplomatic_pact = {
			country = c:D37 #Kuxhekrë
			type = vassal
		}
	}

	c:D12 = { #Hul-Jorkad
		create_diplomatic_pact = {
			country = c:D14 #Ovdal Lodhum
			type = protectorate
		}
	}

	c:D18 = { #Arg Ordstun
		create_diplomatic_pact = {
			country = c:D20 #Orlghelovar
			type = dominion
		}
	}

	c:F14 = { #Overclan
		create_diplomatic_pact = {
			country = c:D19 #Shazstundihr
			type = vassal
		}
	}

	c:B05 = { #VG
		create_diplomatic_pact = {
			country = c:C32 #Devand
			type = puppet
		}
	}

	c:B07 = { #Triarchy
		create_diplomatic_pact = { #got them in their sphere during the Anbennarian colonial collapse
			country = c:B71 #Caylen's Reach
			type = protectorate
		}
		create_diplomatic_pact = { #took them under their protection following the Rending
			country = c:B76 #Bokguan
			type = puppet
		}
	}

	c:B82 = { #Einnsag - Autumnal League
		create_diplomatic_pact = {
			country = c:B83 #Sglard
			type = vassal
		}
		create_diplomatic_pact = {
			country = c:B81 #Eighard
			type = vassal
		}
	}
	c:F13 = { #Harpylen
		create_diplomatic_pact = { 
			country = c:F10 #Garlas-Kel
			type = tributary
		}
	}
	c:A30 = {	#Magocratic Demesne-Wyvernheart customs union as Wyvernheart was helped a lot by them
		create_diplomatic_pact = {
			country = c:A26
			type = protectorate
		}
	}

	c:L01 = { #Konolkhatep
		create_diplomatic_pact = { 
			country = c:L03 #Vogkeysa
			type = vassal
		}
		create_diplomatic_pact = { 
			country = c:L21 #Kulugiash
			type = vassal
		}
	}

	c:L27 = { #Melakmengi
		create_diplomatic_pact = { 
			country = c:L29 #Naleni
			type = vassal
		}
		create_diplomatic_pact = { 
			country = c:L30 #Sky Soaring Eagle
			type = vassal
		}
	}

}