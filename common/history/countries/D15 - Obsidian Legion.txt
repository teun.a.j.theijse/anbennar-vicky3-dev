﻿COUNTRIES = {
	c:D15 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = bessemer_process
		add_technology_researched = mechanical_tools
		add_technology_researched = fractional_distillation
		add_technology_researched = cotton_gin
		add_technology_researched = lathe
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		add_technology_researched = napoleonic_warfare
		add_technology_researched = army_reserves
		add_technology_researched = logistics
		add_technology_researched = fieldworks
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = colonization
		add_technology_researched = academia
		add_technology_researched = mass_communication
		add_technology_researched = medical_degrees
		add_technology_researched = romanticism
		add_technology_researched = empiricism
		add_technology_researched = nationalism
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_ethnostate
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_colonial_exploitation
		activate_law = law_type:law_dedicated_police
		activate_law = law_type:law_religious_schools
		# No health system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slave_trade

		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_embraced
		activate_law = law_type:law_amoral_artifice_embraced
		activate_law = law_type:law_traditional_magic_only


		add_journal_entry = { type = je_third_obsidian_invasion }
		add_modifier = {
			name = obsidian_invasion_modifier
			years = 2
			is_decaying = yes
		}
		trigger_event = { 
			id = obsidian_invasions.1
			days = 12
		}
	}
}