﻿COUNTRIES = {
	c:B07 ?= {
		effect_starting_technology_tier_1_tech = yes
		effect_starting_artificery_tier_1_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_technocracy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_resettlement
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_no_police
		activate_law = law_type:law_public_schools
		# No healthcare
		activate_law = law_type:law_homesteading
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_only

		set_institution_investment_level = {
			institution = institution_colonial_affairs
			level = 2
		}
		set_institution_investment_level = {
			institution = institution_schools
			level = 1
		}

		add_modifier = {
			name = modifier_tianlou_rending_repayments_4
		}
	}
}