﻿COUNTRIES = {
	c:D31 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = cotton_gin
		add_technology_researched = lathe
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		add_technology_researched = napoleonic_warfare #dak influence
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = colonization
		add_technology_researched = academia
		
		
		activate_law = law_type:law_magocracy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_peasant_levies
		# No home affairs

		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		# No colonial affairs
		activate_law = law_type:law_dedicated_police
		# No schools
		# No health system
		
		activate_law = law_type:law_outlawed_dissent
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slave_trade

		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_embraced
		activate_law = law_type:law_amoral_artifice_embraced
		activate_law = law_type:law_artifice_encouraged

		set_institution_investment_level = {
			institution = institution_police
			level = 2
		}
	}
}