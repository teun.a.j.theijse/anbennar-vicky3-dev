﻿COUNTRIES = {
	c:B41 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = napoleonic_warfare
		add_technology_researched = empiricism

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_frontier_colonization
		activate_law = law_type:law_no_migration_controls
		
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_artifice_encouraged
		
		# set_institution_investment_level = {
		# 	institution = institution_schools #Very available education
		# 	level = 2
		# }

		set_institution_investment_level = { #Expanse countries have low pop and need it to colonize at a decent speed
			institution = institution_colonial_affairs
			level = 2
		}
	}
}