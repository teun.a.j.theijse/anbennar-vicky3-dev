﻿COUNTRIES = {
	c:L33 ?= {
		effect_starting_technology_tier_5_tech = yes
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_charitable_health_system

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_no_workers_rights
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_encouraged
	}
}