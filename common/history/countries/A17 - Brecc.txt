﻿COUNTRIES = {
	c:A17 ?= {
		effect_starting_technology_tier_1_tech = yes
		effect_starting_artificery_tier_1_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_total_separation
		activate_law = law_type:law_elected_bureaucrats	
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_consumption_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_right_of_assembly
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_pragmatic_artifice	#one of few that start with this
		activate_law = law_type:law_artifice_encouraged	
	}
}