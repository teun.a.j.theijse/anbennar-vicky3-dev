﻿COUNTRIES = {
	c:E02 ?= {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = centralization
		add_technology_researched = cotton_gin
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_pragmatic_artifice
		activate_law = law_type:law_mundane_production
		
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
	}
}