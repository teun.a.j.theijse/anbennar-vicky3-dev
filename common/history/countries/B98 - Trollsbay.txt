﻿COUNTRIES = {
	c:B98 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism

		effect_starting_politics_liberal = yes

		set_market_capital = STATE_ISOBELIN
		
		set_tariffs_export_priority = g:fabric
		set_tariffs_import_priority = g:dye
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_total_separation
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		activate_law = law_type:law_local_police
		activate_law = law_type:law_private_schools
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_artifice_encouraged
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only

		ig:ig_intelligentsia = {
			add_ruling_interest_group = yes
		}

		set_institution_investment_level = {
			institution = institution_schools
			level = 2
		}
	}
}