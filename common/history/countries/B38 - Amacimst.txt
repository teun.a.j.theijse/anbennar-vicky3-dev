﻿COUNTRIES = {
	c:B38 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_oligarchy #Feudal nobility

		activate_law = law_type:law_censorship
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_only #Hostile towards both settlers and Cursed Ones
		activate_law = law_type:law_traditional_magic_only
		activate_law = law_type:law_pragmatic_artifice #Bloodrunes

		set_ruling_interest_groups = {
			ig_landowners
		}
	}
}
