﻿COUNTRIES = {
	c:A22 ?= {
		effect_starting_technology_tier_2_tech = yes	#most advanced out of escann btw
		
		add_technology_researched = percussion_cap # So they can make ammunition

		activate_law = law_type:law_stratocracy	#changed from parliament, because military junta is presidential. parliament means devolved executive power
		activate_law = law_type:law_autocracy	#itsa military dictatorship
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats #itsa military dictatorship		
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_national_guard
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property #Not allowed women in workplace without voting
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production		#the verdancy made them adverse to magic at game start
	
		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
		
		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
		
		# Give the armed forces more strength
		add_modifier = {
			name = age_of_officers_modifier
			months = very_long_modifier_time
			is_decaying = yes
		}
		
		set_military_wage_level = high
	}
}