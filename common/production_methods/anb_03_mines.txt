## Coal Mine
## Iron Mine
pm_deposit_divination_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_coal_add = 45
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_coal_mine = {	
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_coal_add = 115
		}

		level_scaled = {	#TODO: rebalance these values across the board for all mine types
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_coal_add = 15
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_drill_mining_servos_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_coal_add = 35
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_subterrenes_coal_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_coal_add = 55	
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {	#shits dangerous, offset with prosthetic etc artificery!
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}
}

pm_automata_miners = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		early_mechanim
	}

	disallowing_laws = {
		law_industry_banned
	}

	#state_modifiers = {
    	#workforce_scaled = {
    	#	state_pops_assembled_add = 200
    	#}
    #}

		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 5
			}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_foremen = {
	texture = "gfx/interface/icons/production_method_icons/steam_donkey.dds"
		
	unlocking_technologies = {
		advanced_mechanim
	}

	disallowing_laws = {
		law_industry_banned
	}

	#state_modifiers = {
    	#workforce_scaled = {
    	#	state_pops_assembled_add = 440
    	#}
    #}

		
	building_modifiers = {
		workforce_scaled = {
			# input goods					
				goods_input_automata_add = 11
			}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
		}
	}
}

## Iron Mine
pm_deposit_divination_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_iron_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_iron_add = 90
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_iron_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_drill_mining_servos_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_iron_add = 28
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_subterrenes_iron_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_iron_add = 40
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {	#shits dangerous, offset with prosthetic etc artificery!
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}
}

## Lead Mine
pm_deposit_divination_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_lead_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_lead_add = 90
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_lead_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_drill_mining_servos_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_lead_add = 28
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_subterrenes_lead_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_lead_add = 40	
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}
}

## Sulfur Mine
pm_deposit_divination_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_sulfur_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_sulfur_add = 95
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_sulfur_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_drill_mining_servos_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_sulfur_add = 28
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_subterrenes_sulfur_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_sulfur_add = 40	
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}
}

## Gold Mine
pm_deposit_divination_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_gold_add = 15	#equal to atmospheric engine
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 500
		}
	}
}

pm_transmutative_heavy_pump_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_gold_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 1200
		}
	}
}

pm_evocation_spells_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_gold_add = 5
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 125	#equivalent to nitroglycerin
		}
	}	
}

pm_drill_mining_servos_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_gold_add = 10	
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 500
		}
	}
}

pm_subterrenes_gold_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes		
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_gold_add = 15
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 750
		}
	}
}


## Mithril Mine
pm_picks_and_shovels_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 15 #normal flawless metal mining is 20
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
	}
}
pm_atmospheric_engine_pump_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"

	unlocking_technologies = {
		atmospheric_engine
	}
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 30 #normal flawless metal mining is 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}
pm_condensing_engine_pump_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"

	unlocking_technologies = {
		watertube_boiler
	}
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_flawless_metal_add = 50 #normal flawless metal mining is 60
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
		}
	}
}
pm_diesel_pump_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		compression_ignition
	}

	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 60 #normal flawless metal mining is 80
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 2250
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 750
		}
	}
}

pm_nitroglycerin_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 8 #normal flawless metal mining is 10
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}
pm_dynamite_building_mithril_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 15 #normal flawless metal mining is 20
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

#TODO: add rest of anbennar PMs to mithril mines



## Gem Mine
pm_picks_and_shovels_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_magical_reagents_add = 14
			goods_output_gold_add = 3
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 90 #30 per
		}
	}
}
pm_atmospheric_engine_pump_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_magical_reagents_add = 24
			goods_output_gold_add = 6
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 180
		}
	}	
}
pm_condensing_engine_pump_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_magical_reagents_add = 42
			goods_output_gold_add = 9
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 270 
		}
	}	
}
pm_diesel_pump_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		compression_ignition
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			
			# output goods
			goods_output_magical_reagents_add = 49
			goods_output_gold_add = 12
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 2250
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 750
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 360
		}
	}
}
pm_transmutative_heavy_pump_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		morphic_transmutation
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_magical_reagents_add = 63
			goods_output_gold_add = 15
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 2000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 1000
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 450
		}
	}
}

pm_nitroglycerin_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
	 	nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_magical_reagents_add = 8	
			goods_output_gold_add = 2
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 60
		}
	}
}

pm_dynamite_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_magical_reagents_add = 14
			goods_output_gold_add = 3
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 90
		}
	}
}	

pm_drill_mining_servos_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		thinking_servos
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_magical_reagents_add = 20
			goods_output_gold_add = 5
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 150
		}
	}
}	

pm_subterrenes_building_gem_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		subterrenes
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_magical_reagents_add = 28
			goods_output_gold_add = 6
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 240
		}
	}
}	

## Damestear Mines
pm_picks_and_shovels_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_damestear_add = 20 #note: since i made damestear as expensive as sulfur (most expensive vanilla raw resource), i also changed its mines to output damestear at the same rate
			# damestear is still INCREDIBLY limited, but there's now enough of it to actually have a damestear economy (hopefully)
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
	}
}

pm_atmospheric_engine_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_damestear_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

pm_condensing_engine_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_damestear_add = 60
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
		}
	}
}

pm_diesel_pump_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		combustion_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			
			# output goods
			goods_output_damestear_add = 80
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 2250
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 750
		}
	}
}

pm_deposit_divination_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_damestear_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_transmutative_heavy_pump_damestear_mine = {
	texture ="gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_damestear_add = 95
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_damestear_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_nitroglycerin_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
	 	nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_damestear_add = 10
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}

pm_dynamite_building_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_damestear_add = 20
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_drill_mining_servos_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_damestear_add = 28
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_subterrenes_damestear_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 5
			
			# output goods
			goods_output_damestear_add = 40
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}
}


## Damestear Fields
default_building_damestear_fields = {
	texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_damestear_add = 40 # Damestear is half the price of gold. Gold fields produce 20 gold. Therefore, damestear fields produce 40 damestear
		}
		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
	}
}

## Flawless Metal Mines
pm_picks_and_shovels_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 20
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
		}
	}
}

pm_deposit_divination_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_magical_reagents_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 4500
			building_employment_mages_add = 500
		}
	}
}

pm_atmospheric_engine_pump_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/pumps.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
		atmospheric_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 10
			goods_input_coal_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 40
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3750
			building_employment_machinists_add = 500
			building_employment_engineers_add = 250
		}
	}
}

pm_condensing_engine_pump_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/condensing_engine_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_coal_add = 15
			
			# output goods
			goods_output_flawless_metal_add = 60
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 3000
			building_employment_machinists_add = 1000
			building_employment_engineers_add = 500
		}
	}
}

pm_diesel_pump_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}

	unlocking_technologies = {
		combustion_engine
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 80
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 2250
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 750
		}
	}
}

pm_transmutative_heavy_pump_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/diesel_pump.dds"

	unlocking_technologies = {
		morphic_transmutation
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 15
			goods_input_oil_add = 5
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 95
		}

		level_scaled = {
			building_employment_shopkeepers_add = 500
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 1750
			building_employment_engineers_add = 750
		}
	}
}

pm_evocation_spells_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 7
			
			# output goods
			goods_output_flawless_metal_add = 10
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}
}

pm_nitroglycerin_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/nitroglycerin.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 5
		}
	}

	unlocking_technologies = {
	 	nitroglycerin
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 6
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.3
			building_machinists_mortality_mult = 0.2
			building_engineers_mortality_mult = 0.1
		}
	}
}

pm_dynamite_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/dynamite.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}

	unlocking_technologies = {
		dynamite
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_explosives_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 14
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_drill_mining_servos_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"
	unlocking_technologies = {
		thinking_servos
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 12
			goods_input_artificery_doodads_add = 3
			
			# output goods
			goods_output_flawless_metal_add = 20
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}
	}
}

pm_subterrenes_flawless_metal_mine = {
	texture = "gfx/interface/icons/production_method_icons/tractors.dds"
	unlocking_technologies = {
		subterrenes			
	}
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 14
			goods_input_artificery_doodads_add = 6
			goods_input_engines_add = 2
			
			# output goods
			goods_output_flawless_metal_add = 30
		}

		level_scaled = {
			building_employment_engineers_add = 250
		}

		unscaled = {
			building_laborers_mortality_mult = 0.4
			building_machinists_mortality_mult = 0.3
			building_engineers_mortality_mult = 0.2
		}
	}
}

pm_sintering_forges = {
	texture = "gfx/interface/icons/production_method_icons/blister_steel_process.dds"
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 10
		}
	}
}

pm_lava_pumped_forges = {
	texture = "gfx/interface/icons/production_method_icons/bessemer_process.dds"
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 15
		}
	}
	
	unlocking_technologies = {
		watertube_boiler
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_tools_add = 5
			goods_input_coal_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 10
		}
		unscaled = {
			building_laborers_mortality_mult = 0.05
			building_machinists_mortality_mult = 0.05
			building_engineers_mortality_mult = 0.025
		}
	}
}

pm_fire_elemental_forges = {
	texture = "gfx/interface/icons/production_method_icons/open_hearth_process.dds"
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 20
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 15
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_flawless_metal_add = 25
		}
		unscaled = {
			building_laborers_mortality_mult = 0.1
			building_machinists_mortality_mult = 0.1
			building_engineers_mortality_mult = 0.05
		}
	}
}

pm_hypercharged_electric_arc_forges = {
	texture = "gfx/interface/icons/production_method_icons/electric_arc_process.dds"
	
	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 25
		}
	}
	
	unlocking_technologies = {
		arc_welding
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 15
			goods_input_electricity_add = 15 
			goods_input_artificery_doodads_add = 10
			
			# output goods
			goods_output_flawless_metal_add = 40
		}
		unscaled = {
			building_laborers_mortality_mult = 0.1
			building_machinists_mortality_mult = 0.1
			building_engineers_mortality_mult = 0.05
		}
	}
	required_input_goods = electricity
}

pm_dense_medium_separation = {
	texture = "gfx/interface/icons/production_method_icons/assembly_lines.dds"
	
	unlocking_technologies = {
		electrical_generation
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_steel_add = 4
			goods_input_electricity_add = 4
		}

		level_scaled = {
			building_employment_laborers_add = -1500
		}
	}

	required_input_goods = electricity
}

pm_froth_flotation = {
	texture = "gfx/interface/icons/production_method_icons/assembly_lines.dds"
	
	unlocking_technologies = {
		artificial_life
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_electricity_add = 5
			goods_input_oil_add = 5
			goods_input_explosives_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = -2000
			building_employment_machinists_add = -1000
		}
	}

	required_input_goods = electricity
}