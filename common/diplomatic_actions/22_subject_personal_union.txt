﻿personal_union = {
	groups= {
		general # for breaking only
	}
	show_in_lens = no

	potential = { # Only created through diplomatic plays
		always = no
	}
	
	pact = {
		cost = 25
		has_junior_participant = yes
		recipient_pays_maintenance = no
		
		relations_progress_per_day = 1
		relations_improvement_max = 50
		
		subject_type = subject_type_personal_union
		
		second_modifier = {
			country_law_enactment_imposition_success_add = 0.1
			country_convoy_contribution_to_market_owner_add = 0.5
		}
		
		actor_can_break = {
			is_diplomatic_play_committed_participant = no
		}

		target_can_break = { # Has to use diplomatic play to break
			always = no
		}	

		requirement_to_maintain = {	#Anbennar 
			trigger = { 
				OR = {
					has_law = law_type:law_monarchy
					has_government_type = gov_jadd_empire_theocracy
				}
			}
		}
		
		requirement_to_maintain = { #Anbennar
			trigger = {
				scope:target_country = {
					OR = {
						has_law = law_type:law_monarchy
						has_government_type = gov_jadd_empire_theocracy
					}
				}
			}
		}	
		
		manual_break_effect = {
			create_bidirectional_truce = { country = scope:target_country months = 60 }
		}
		
		auto_break_effect = {
			create_bidirectional_truce = { country = scope:target_country months = 60 }
		}		
	}
	
	ai = {
		evaluation_chance = {
			value = 0
		}		
	
		will_break = {
			always = no			
		}		
	}
}
