﻿# TODO - look at the modifiers in these files, despite base chances being extremely low there's still a tonne of racial purists etc... about

ideology_aakhet = {
	icon = "gfx/interface/icons/ideology_icons/ideology_leader/ideology_leader_jingoist.dds" 
	
	character_ideology = yes

	lawgroup_governance_principles = {
		law_monarchy = neutral
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_theocracy = neutral
		law_council_republic = neutral

		#Anbennar
		law_magocracy = neutral
		law_stratocracy = approve
	}

	lawgroup_army_model = {
		law_professional_army = strongly_approve
		law_mass_conscription = approve
		law_national_militia = neutral
		law_peasant_levies = strongly_disapprove
	}

	lawgroup_internal_security = {
		law_national_guard = strongly_approve
		law_no_home_affairs = neutral
		law_secret_police = neutral
		law_guaranteed_liberties = disapprove
	}

	lawgroup_policing = {
		law_militarized_police = strongly_approve
		law_dedicated_police = approve
		law_local_police = neutral
		law_no_police = disapprove
	}

	lawgroup_slavery = {
		law_slave_trade = approve
		law_debt_slavery = approve
		law_legacy_slavery = neutral
		law_slavery_banned = disapprove
	}

	possible = {
		always = no
	}
	
	leader_weight = {
		value = 1
	}
}
