﻿pmg_mining_equipment_building_damestear_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_picks_and_shovels_building_damestear_mine
		pm_deposit_divination_damestear_mine
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
		pm_transmutative_heavy_pump_damestear_mine
	}
}

pmg_explosives_building_damestear_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_explosives
		pm_evocation_spells_damestear_mine
		pm_nitroglycerin_building_damestear_mine
		pm_dynamite_building_damestear_mine
		pm_drill_mining_servos_damestear_mine
		pm_subterrenes_damestear_mine
	}
}

pmg_steam_automation_building_damestear_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_steam_automation
		pm_automata_miners
		pm_steam_donkey_mine
		pm_automata_foremen
	}
}

pmg_train_automation_building_damestear_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_road_carts
		pm_rail_transport_mine
	}
}


pmg_base_building_damestear_fields = {
	production_methods = {
		default_building_damestear_fields
	}
}

pmg_mining_equipment_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_picks_and_shovels_building_flawless_metal_mine
		pm_deposit_divination_flawless_metal_mine
		pm_atmospheric_engine_pump_building_flawless_metal_mine
		pm_condensing_engine_pump_building_flawless_metal_mine
		pm_diesel_pump_building_flawless_metal_mine
		pm_transmutative_heavy_pump_flawless_metal_mine
	}
}

pmg_explosives_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_explosives
		pm_evocation_spells_flawless_metal_mine
		pm_nitroglycerin_building_flawless_metal_mine
		pm_dynamite_building_flawless_metal_mine
		pm_drill_mining_servos_flawless_metal_mine
		pm_subterrenes_flawless_metal_mine
	}
}

pmg_forging_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_sintering_forges
		pm_lava_pumped_forges
		pm_fire_elemental_forges
		pm_hypercharged_electric_arc_forges
	}
}

pmg_automation_building_flawless_metal_mine = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_automation
		pm_dense_medium_separation
		pm_froth_flotation
	}
}