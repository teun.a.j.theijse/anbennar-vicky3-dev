﻿ig_trait_family_ties_witch_king = {
	icon = "gfx/interface/icons/ig_trait_icons/family_ties.dds"
	min_approval = happy
	
	modifier = {
		building_group_bg_agriculture_throughput_add = 0.05
		building_group_bg_ranching_throughput_add = 0.05
		building_group_bg_plantations_throughput_add = 0.05
	}
}

ig_trait_ministerial_duty = {
	icon = "gfx/interface/icons/ig_trait_icons/divine_right.dds"
	min_approval = happy
	
	modifier = {
		country_bureaucracy_mult = 0.1
	}
}

ig_trait_the_fine_points_of_administration = {
	icon = "gfx/interface/icons/ig_trait_icons/noblesse_oblige.dds"
	min_approval = loyal
	
	modifier = {
		state_tax_capacity_mult = 0.15
	}
}

ig_trait_societal_cohesion = {
	icon = "gfx/interface/icons/ig_trait_icons/avant_garde.dds"
	min_approval = happy
	
	modifier = {
		state_decree_cost_mult = -0.1
	}
}